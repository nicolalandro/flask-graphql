from graphene import ObjectType, String, Schema


class ExampleQuery(ObjectType):
    hello = String()
    xxx = String()

    def resolve_hello(self, info):
        return "Hello"

    def resolve_xxx(self, info):
        return "come se fosse antani"


class RootQuery(ExampleQuery, ObjectType):
    pass


schema = Schema(query=RootQuery)
