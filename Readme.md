Seguendo il [Tutorial](https://medium.com/free-code-camp/how-to-develop-a-flask-graphql-graphene-mysql-and-docker-starter-kit-4d475f24ee76).

# Comandi utili
* run server:
```bash
python ./manage.py runserver
```
# Utilizzo
```bash
docker-compose up
firefox localhost:5000/graphql
```
* QUERY
```graphql
{
  hello,
  xxx
}
```